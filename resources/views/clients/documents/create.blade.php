@extends('layouts.app')


<script src="{{ asset('js/app.js') }}" defer></script>
@section('content')


<div class="container">       
    <h1>Fill Document</h1>

        
        <form action="{{ route('clients.documents.store',Request::segment(2)) }}" method="post">
        @csrf
        
        {{-- @error('name')
        <p class="text-red-500 text-xs">{{ $message }}</p>
        @enderror --}}

        @forelse ($errors->all() as $message)
        <p class="alert alert-danger">{{ $message }}</p>
        @empty
            
        @endforelse
   
        
        <div class="row">
            <div class="col">
                <label for="a4">Select Document Type</label>
                <div id="app">
                    <fields-data></fields-data>
                </div>
            </div>
            <div class="col">
                <label for="a1">Document Number</label>
                <input type="number" name="documentNumber" id="a1">
            </div>
            <div class="col">
                <label for="a2">Document Date</label>
                <input type="date" name="date" id="a2">
            </div>
        </div>


    
        
        <input type="hidden" name="upload" value="{{ $uploadId }}">
        <input type="submit" value="Fill Document">
    </form>
    

</div>



@endsection
