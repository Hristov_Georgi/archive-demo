<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', function () {return view('welcome');});

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function(){
    
    Route::get('file/view/{file}', 'FileUploadController@serveFile')->name('serve-file');
    Route::get('file/download/{file}', 'FileUploadController@downloadFile')->name('download-file');
    Route::resource('uploads', 'FileUploadController');

    // Route::resource('clients.documents', 'ClientDocumentController')->shallow();
    Route::get('/test','ClientDocumentController@getType');
    Route::resource('clients.documents', 'ClientDocumentController')->scoped();

    Route::get('datatable/{clientId}', 'ClientController@datatables')->name('clients.datatable');
    Route::resource('clients', 'ClientController');

    Route::apiResource('upload', 'DropzoneController'); //TODO: refactor by moving to API/ subfolder
    
    Route::get('stored-docs/{company:company_name}', 'StoredDocsController@show')->name('stored-docs.show');
    Route::get('fetch-doc/{client}', 'StoredDocsController@datatables')->name('stored-docs.resource');
});