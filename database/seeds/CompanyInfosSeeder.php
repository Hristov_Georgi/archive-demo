<?php

use App\CompanyInfos;
use Illuminate\Database\Seeder;

class CompanyInfosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CompanyInfos::class,5)->create();
    }
}
