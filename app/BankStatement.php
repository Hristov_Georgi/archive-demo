<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankStatement extends Model
{

    protected $fillable = [
        'bank_name','iban'
    ];

    public $fields = [
        'bank name',
        'iban'
    ];

    public function getFields()
    {
        return [
            'bank name',
            'iban'
        ];
    }

    public function document()
    {
        return $this->morphOne('App\Document','typeable');
    }
}
