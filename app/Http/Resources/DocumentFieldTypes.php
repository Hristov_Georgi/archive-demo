<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentFieldTypes extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return [
        //     'id' => $this->id,
        //     'path' => $this->path,
        //     'fileType' => $this->extension,
        //     'companyId' => $this->company_id,
        //     'accounted__for' => $this->accounted_for
        // ];
        return parent::toArray($request);
    }
}
