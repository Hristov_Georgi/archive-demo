<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Hello, {{ $credentials['ownerName']}}! 
    <p>You can log into your account using the password:</p>
    <p>{{ $credentials['password']}}</p>
    <p>at this adress <a href="">here</a>!</p>
</body>
</html>