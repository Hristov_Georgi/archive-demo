<?php

namespace App\Http\Controllers;

use App\CompanyInfos;
use App\Document;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class StoredDocsController extends Controller
{
    public function show(CompanyInfos $company)
    {
        // dd($company);
        $this->authorize('viewUserStoredDocs', $company );
        // $this->authorize('show-user-stored-docs', $company );

        $companyDocs = Document::with('typeable')->where('company_id',$company->id)->get(); //eager loaded
        
        return view('stored-docs.show', compact('companyDocs','company'));
        
    }

    public function datatables(CompanyInfos $client)
    {   

        $companyDocs = Document::with('typeable')->where('company_id',$client->id)->get();

        return DataTables::of($companyDocs)
        ->addColumn('created_at', function($model) {
            return $model->created_at->diffForHumans();
        })
        ->addColumn('typeable_type', function($model) {
            return $model->getTypeName();
        })
        // ->addColumn('details', function($model) {
        //     return ($model->typeable);
        // })
        ->make(true);

    }
}
