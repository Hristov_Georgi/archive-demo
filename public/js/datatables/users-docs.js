
$(document).ready(function () {
    
    let isClient = $('#isClient').val() == 1 ? true : false;
    $('#company_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/datatable/'+ $('#clientId').val(),
            type: 'GET',
            // data: { 'clientId': $('#clientId').val() },
        },
        columns: [
            {
                data: "extension",
                name: "extension",
                render: function (data,type,row) {
                    
                    return data.toUpperCase()
                }
            },
            {
                data: "created_at",
                name: "created_at"
            },
            {
                name: "accounted_for",
                data: "accounted_for",
                render: function ( data,type,row ) {

                    if (data == 1) {
                        return '<p class="btn btn-success" style="cursor:default">Yes</p>';
                    } else if (data != 1 && isClient == true) {
                        return '<p class="btn btn-warning" style="cursor:default">Pending</p>';
                    }
                    clientId = $('#clientId').val();
                    docId = row['id'];
                    return '<a href="' + clientId + '/documents/create?upload='+ docId +'" class="btn btn-warning">Fill It</a>';
                }
            },
            {
                data: "action",
                name: "action",
                orderable: false
            },
            {
                data: "action2",
                name: "action2",
                orderable: false
            }
        ],
        columnDefs: [{
            targets: '_all',
            defaultContent: 'default content'
        }],
        scrollY: '350px'
    })
});
