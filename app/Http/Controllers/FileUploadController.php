<?php

namespace App\Http\Controllers;

use App\FileUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileUploadController extends Controller
{
    public function index()
    {
        $client = getCurrentUserCompany();
        $uploads = getCurrentUserCompany()->uploads;


        return view('clients.show',compact(['uploads', 'client'])); // return the same datatable as admin's user's docs

        // return view('uploads.index', compact(['uploads', 'client']));
    }

    public function serveFile(FileUpload $file)
    {
        $mime = mime_content_type(storage_path('app/' . $file->path));
        
        return response(
            Storage::get($file->path),
            200,
            [
                'Content-Type' => $mime,
                'Content-Disposition' => 'inline; ' . $file->path,

            ]
        );
    }

    public function downloadFile(FileUpload $file)
    {
        return response()->download(storage_path('app/' . $file->path));
    }
}
