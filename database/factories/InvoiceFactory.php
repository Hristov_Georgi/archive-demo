<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Document;
use App\Invoice;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        'tax_base' => $faker->numberBetween(0,100),
        'currency' => $faker->currencyCode,
        'vat' => $faker->boolean(),
        'price' => $faker->numberBetween(1,200000),
        // 'document_id' => 1,
        // 'document_id' => Document::orderByRaw('RAND()')->first()->id
    ];
});
