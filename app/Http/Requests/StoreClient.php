<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:25',
            'companyName' => 'required|string|min:3|max:25|unique:company_infos,company_name',
            'ownerName' => 'required|string',
            'email' => ['email','string','required','unique:users,email'],
            'bulstat' => ['required','numeric']
            
        ];
    }
}
