@extends('layouts.app')

@push('datatables')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
    
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js" type="text/javascript" defer></script>

@endpush

@section('content')


    <div class="container">
        
        <h1>{{ $company->company_name }}'s details</h1>
        <div class="row">
            <div class="col-sm my-3">
                <p>Company Name: {{ $company->company_name }}</p>
            </div>
            <div class="col-sm my-3">
                Owner: {{ $company->owner }}
            </div>
            <div class="col-sm my-3">
                <p>Bulstat: {{ $company->bulstat }}</p>
            </div>
            <div class="col-sm my-3">
                <p>Email: {{ $company->getAccountInfo->email }}</p>
            </div>
        </div> 
        
        <table id="stored_docs_table">
            <thead>
            <tr>
                <th>Doc's Date</th>
                <th>Number</th>
                <th>Type</th>
                <th>Type Details</th>
                <th>Accounted On</th>
            </tr>
            </thead>
        </table>
    </div>

    <input type="hidden" id="clientId" value="{{ $company->id }}">
    <script src="{{ asset('js/datatables/stored-docs.js') }}"></script>
    
@endsection
    