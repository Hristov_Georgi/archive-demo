<?php

namespace App\Http\Controllers;


use App\CompanyInfos;
use Illuminate\Support\Str;     
use App\Http\Requests\StoreClient;
use App\Mail\RegistrationCredentials;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;

class ClientController extends Controller
{

    public function __construct()
    {
        // $this->middleware('admin')->except(['show']);
        $this->authorizeResource(CompanyInfos::class, 'client');
    }
   
    public function index()
    {
        $clients = CompanyInfos::where('accountant_id',currentUser()->id)->get();
    
        if (request()->ajax()) {
            return DataTables::of($clients)
                    ->addColumn('action', function($data){
                        return view('clients.partial.view', compact('data'));
                        // $button = '<button type="button"
                        //             name="edit" id="'.$data->id.'"
                        //             class="edit btn btn-primary 
                        //             btn-sm">Edit</button>';
                        // $button .= '
                        //     &nbsp;&nbsp;&nbsp;<button
                        //     type="button" name="edit" id="'.$data->id.'"
                        //     class ="delete btn btn-danger btn-sm">Delete</button';
                        //     return $button;
                    })
                    ->addColumn('unfilled', function($client){
                        return $client->uploads()->where('accounted_for',0)->count();
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
       
        return view('clients.index', compact('clients'));
    }

    public function show(CompanyInfos $client)
    {
        // $this->authorize('view',$client);
        $uploads = $client->uploads;
        // dd($uploads,$client);

        return view('clients.show',compact(['client','uploads']));
    }

    public function store(StoreClient $request)
    {
        $data = $request->validated();
        // dd($data);
        $password = Str::random(8);
        
        $mailable = $data;
        $mailable['password'] = $password;
       
        User::create([
            'name' => $data['companyName'],
            'email' => $data['email'],
            'password' => Hash::make($password),
            // 'accountant_id' => auth()->user()->id,
            'isClient' => true,
        ]);

        CompanyInfos::create([
            'company_name' => $data['companyName'],
            'owner' => $data['ownerName'],
            'bulstat' => $data['bulstat'],
            'accountant_id' => auth()->user()->id,
            'user_id' => User::where('email',$data['email'])->get()[0]->id
        ]);
            
        Mail::to($data['email'])->send(new RegistrationCredentials($mailable));

        return back()->with('message','Client Registered');
    }

    public function datatables(CompanyInfos $clientId)
    {   
        // dd($clientId);
        $clientUploads = $clientId->uploads();


        return DataTables::of($clientUploads)
        ->addColumn('created_at', function($model) {
            return $model->created_at->diffForHumans();
        })
        ->addColumn('action', function($upload){
            return view('admin.partials.show-file', compact('upload'));
        })
        ->addColumn('action2', function($upload){
            return view('admin.partials.download-file', compact('upload'));
        })
        ->rawColumns(['action'])
        ->rawColumns(['action2'])
        ->make(true);

    }

}
