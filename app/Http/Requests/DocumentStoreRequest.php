<?php

namespace App\Http\Requests;

use App\CompanyInfos;
use App\Document;
use App\FileUpload;
use App\Rules\NumberRange;
use App\Rules\PositiveNumber;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DocumentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return true;
        $uploadObj = FileUpload::find(request('upload'));
        // dd($uploadObj,[Document::class,request('upload')]);
        return currentUser()->can('create',[Document::class,$uploadObj]);
        // return currentUser()->can('create',[Document::class,request('upload')]);
    }


    public function attributes()
    {
        return [
            'documentNumber' => '\'document number\'',
            'taxBase' => "'tax base'",
            'paymentType' => '\'payment type\'',
            'bankName' => "'bank name'",

        ];
    }


        public function messages()
    {
        return [
            //  example
            // 'date.required' => 'A date must be given for a document',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $params = request()->all();
        // dd($params);
        return [
            
            // General 
            'docType' =>  Rule::in([1, 2, 3]),
            'upload' => ['required','exists:file_uploads,id', /* (function ($query) use ($params) {
                $fileOwner = FileUpload::find($params['upload'])->owner;
                return $fileOwner->accountant_id == currentUser()->id;
            }) */],
            'date' => ['required','date'],
            'documentNumber' => ['required','numeric'],

            // Invoice
            'taxBase' => ['bail','exclude_unless:docType,1','required','numeric',new PositiveNumber,new NumberRange],
            'vat' => ['bail','exclude_unless:docType,1','required','numeric',new PositiveNumber,new NumberRange],        
            'currency' => ['bail','exclude_unless:docType,1','required'],
            'price' => ['bail','exclude_unless:docType,1','required','numeric'],

            // Bank Statement
            'bankName' => ['exclude_unless:docType,2','required','string'],                         
            'iban' => ['exclude_unless:docType,2','required'],

             // Creadit Note
            'note' => ['exclude_unless:docType,3','required','string'],
            'paymentType' => ['exclude_unless:docType,3','required',Rule::in([1,2])],

        ];

    }
}
