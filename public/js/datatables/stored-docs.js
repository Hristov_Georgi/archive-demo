$(document).ready(function () {
    $('#stored_docs_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/fetch-doc/' + $('#clientId').val(),
            type: 'GET'
        },
        columns: [

            

            {data: 'date', name: 'date'},
            
            {data: 'number', name: 'number'},

            
            {data: 'typeable_type', name: 'typeable_type'},

            {
                // data: 'details',
                // name: '',

                "render": function ( data, type, row, meta )
                {
                    // console.log(row.typeable)

                    if (row.typeable_type == 'Invoice') {
                        return "Tax Base: " + row.typeable.tax_base + "<br>" +
                               "VAT: " + row.typeable.vat + "<br>" +
                               "Currency: " + row.typeable.currency + "<br>" +
                               "Price: " + row.typeable.price;

                    } else if (row.typeable_type == 'Bank Statement'){
                        return "Bank Name: " + row.typeable.bank_name + "<br>" +
                               "IBAN: " + row.typeable.iban;
                              
                    } else if (row.typeable_type == 'Credit Note'){
                        
                        return "Payment Type: " + (row.typeable.payment_type == 1 ? 'Cash' : 'Bank')
                                + "<br>" +
                                "Note: " + row.typeable.note;
                    }
                
                }
            },
            
            {data: 'created_at', name: 'created_at'},
            
        ],
        
        columnDefs: [ {
                "targets": 3,
                 "orderable": false
              } ],
        scrollY: "350px"
    })
});