<div class="container">
    <h1>{{ $client->company_name }}'s uploaded documents</h1>
    <div class="row">
        <div class="col-sm my-3">
            <p class="lead">File Type</p>  
        </div>
        <div class="col-sm my-3">
             <p class="lead">Uploaded On</p>
        </div>
        <div class="col-sm my-3">
            <p class="lead">Accounted For</p>  
        </div>
        <div class="col-sm my-3">
            <p class="lead">View / Download File</p>    
        </div>
    </div> 
@forelse ($uploads as $upload)
    <div class="row">
        <div class="col-sm my-3">
            {{ strtoupper($upload->extension) }}
        </div>
        <div class="col-sm my-3">
            {{ $upload->created_at->diffForHumans() }}
        </div>
        <div class="col-sm my-3">
           {{--  TODO: check if working  if admin replace with anchor tag linking to eidt document --}}
   
            @if (!currentUser()->isClient)
                @if ($upload->accounted_for)
                    <p class="btn btn-success" style="cursor:default">Yes</p>
                @else
                    <a href="{{ route('clients.documents.create',[$upload->owner,'upload' => $upload->id]) }}" class="btn btn-warning">Fill It</a>
                @endif
            @else
                @if ($upload->accounted_for)
                    <p class="btn btn-success" style="cursor:default">Yes</p>
                @else
                    <p class="btn btn-warning" style="cursor:default">Pending</p>
                @endif
            @endif



        </div>
        <div class="col-sm my-3">
            <span><a href="{{ route('serve-file',$upload->id) }}" target="_blank">View</a>
                /
            <a href="{{ route('download-file',$upload->id) }}">Download</a> </span> 
        </div>
    </div> 
@empty
    <p>No files uploaded yet</p>
@endforelse