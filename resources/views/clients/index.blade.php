@extends('layouts.app')

@push('datatables')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js" type="text/javascript" defer></script>

@endpush

@section('content')


    <div class="container">
        <div class="mb-3">
            @include('clients.register-client-modal')
        </div>

        <table id="clients-table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Company Name</th>
                <th>Bulstat</th>
                <th>Action</th>
                <th>Unfilled</th>
            </tr>
            </thead>
        </table>
    </div>


    <script src="{{ asset('js/datatables/users.js') }}"></script>


@endsection