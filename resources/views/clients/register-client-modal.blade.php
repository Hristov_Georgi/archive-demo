 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#registerModal" data-whatever="@getbootstrap">Register New Client</button>

<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body">
            <form id="client-form" method="POST" action="{{ route('clients.store') }}">
                @csrf
                <div class="form-group">
                    <label for="name-label" class="col-form-label">Name:</label>
                    <input type="text" name="name" class="form-control" id="name-label">
                </div>
                <div class="form-group">
                    <label for="company-label" class="col-form-label">Company name:</label>
                    <input type="text" name="companyName" class="form-control" id="company-label">
                </div>
                <div class="form-group">
                    <label for="owner-label" class="col-form-label">Owner's name:</label>
                    <input type="text" name="ownerName" class="form-control" id="owner-label">
                </div>
                <div class="form-group">
                    <label for="bulstat-label" class="col-form-label">Bulstat:</label>
                    <input type="text" name="bulstat" class="form-control" id="bulstat-label">
                </div>
                <div class="form-group">
                    <label for="email-label" class="col-form-label">Email:</label>
                    <input type="email" name="email" class="form-control" id="email-label">
                </div>
            </form>
        </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <input type="submit" form="client-form" class="btn btn-primary" value="Register">
            </div>
        </div>
    </div>
</div>
