@extends('layouts.app')

@push('datatables')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
    
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js" type="text/javascript" defer></script>

@endpush

@section('content')


<div class="container">

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif


    <h1>Client's details</h1>
    <div class="row">
        <div class="col-sm my-3">
            <p>Company Name: {{ $client->company_name }}</p>
        </div>
        <div class="col-sm my-3">
            Owner: {{ $client->owner }}
        </div>
        <div class="col-sm my-3">
            <p>Bulstat: {{ $client->bulstat }}</p>
        </div>
        <div class="col-sm my-3">
            <p>Email: {{ $client->getAccountInfo->email }}</p>
        </div>
    </div> 

    {{-- <a href="{{ route('documents.index') }}" role="button" type="button" class="btn btn-danger">See All Unaccounted Docs</a> --}}

    <div class="container">
        <table id="company_table">
            <thead>
            <tr>
                <th>File Type</th>
                <th>Uploaded On</th>
                <th>Accounted For</th>
                <th>View</th>
                <th>Download</th>
            </tr>
            </thead>
        </table>
    </div>

</div>




{{-- old (static) way of rendering information about docs --}}
{{-- <x-user-uploads :client=$client :uploads=$uploads></x-user-uploads> --}}


<input type="hidden" id="clientId" value="{{ $client->id }}">
<input type="hidden" id="isClient" value="{{ currentUser()->isClient }}">


<script src="{{ asset('js/datatables/users-docs.js') }}"></script>


@endsection
