<?php

function currentUser()
{
    return auth()->user();
}

function getCurrentUserCompany()
{
    return currentUser()->getCompanyInfo;
}

function previousPathSegments()
{
    $urlString = session('_previous')['url'];
    $url = $urlString;
    $parse = parse_url($url);
    $segments = explode('/',$parse['path']);
    array_shift($segments);
    
    return $segments;
}