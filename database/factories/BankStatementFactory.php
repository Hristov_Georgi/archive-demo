<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BankStatement;
use App\Document;
use App\Model;
use Faker\Generator as Faker;

$factory->define(BankStatement::class, function (Faker $faker) {
    return [
        'bank_name' => $faker->name(),
        'iban' => $faker->numberBetween(1000000000,2147483647),
        // 'document_id' => 3,
        // 'document_id' => Document::orderByRaw('RAND()')->first()->id,
    ];
});
