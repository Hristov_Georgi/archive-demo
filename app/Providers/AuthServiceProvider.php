<?php

namespace App\Providers;

use App\CompanyInfos;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy', 
        'App\CompanyInfos' => 'App\Policies\ClientPolicy',
        'App\FileUpload' => 'App\Policies\DocumentPolicy',
        // 'App\Document' => 'App\Policies\DocumentPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        Gate::define('viewUserStoredDocs', function (User $currUser, CompanyInfos $company) {
            
            return $currUser->id == $company->user_id; 
        });
    }
}
