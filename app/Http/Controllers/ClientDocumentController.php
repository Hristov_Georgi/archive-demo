<?php

namespace App\Http\Controllers;

use App\BankStatement;
use App\CreditNote;
use App\Invoice;
use App\Document;
use App\FileUpload;
use Illuminate\Http\Request;
use App\Http\Requests\DocumentStoreRequest;
use App\Http\Resources\DocumentFieldTypes;
use App\Policies\DocumentPolicy;

class ClientDocumentController extends Controller
{

    // public function getType()
    // {
    //     $type = request()->docTypes;
    //     $types = ['1' ,'2' ,'3'];

    //     $allUploads = FileUpload::all();

    //     return DocumentFieldTypes::collection($allUploads);
  
     
    //     if (in_array($type, $types)) {
    //         return DocumentFieldTypes::collection($allUploads);
    //     } else {
    //         abort(404);
    //     }

    //     // return new DocumentFieldTypes(User::findOrFail($id));
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        $upload = $request->query('upload');
        $upload = FileUpload::find($upload);
       
        $this->authorize('create',$upload);
        $uploadId = $upload->id;

        return view('clients.documents.create',compact('uploadId'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentStoreRequest $request,$companyId)
    {
        // for testing purpouses
        // $upload = $request->input('upload');
        // $upload = FileUpload::find($upload);
        // $this->authorize('create',$upload);
        


        $attributes = $request->validated();
        // dd($attributes);

        $documentBasicInfo = new Document([
            'number' => $attributes['documentNumber'],
            'date' => $attributes['date'],
            'upload_id' =>  $attributes['upload'],
            'company_id' => $companyId,
        ]);


        if ($attributes['docType'] == 1) {  // Invoice 
            
            $invoiceDetails = Invoice::create([
                'tax_base' => $attributes['taxBase'],
                'currency' => $attributes['currency'],
                'price' => $attributes['price'], 
                'vat' => $attributes['vat'],
            ]);

            $invoiceDetails->document()->save($documentBasicInfo);

        } elseif ($attributes['docType'] == 2) { // Bank Statement
            
            $bankStatementDetails = BankStatement::create([
                'bank_name' => $attributes['bankName'],
                'iban' => $attributes['iban'],
            ]);

            $bankStatementDetails->document()->save($documentBasicInfo);

        } elseif ($attributes['docType'] == 3) { // Credit Note

            $creditNoteDetails = CreditNote::create([
                'note' => $attributes['note'],
                'payment_type' => $attributes['paymentType'],
            ]);

            $creditNoteDetails->document()->save($documentBasicInfo);

        } else {
            return abort(403);
        }

        $file = FileUpload::find($attributes['upload']);
        $file->accounted_for = true;
        $file->save();

        return redirect()
            ->to(route('clients.show',$companyId))
            ->with('message','Successfuly Added To The Register');
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        // 
    }

    
}
