<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInfos extends Model
{
    protected $fillable = [
        'company_name', 'owner', 'bulstat', 'accountant_id', 'user_id'
    ];

    public function accountant()
    {
        return $this->belongsTo(User::class, 'accountant_id');
        // return $this->belongsTo(User::class,'user_id')->where('isClient',NULL);
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'company_id');
    }


    public function getAccountInfo()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function uploads()
    {
        return $this->hasMany(FileUpload::class, 'company_id');
    }


    // public function getRouteKeyName()
    // {
    //     return 'bulstat';
    // }
}
