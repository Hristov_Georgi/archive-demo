<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CreditNote;
use App\Document;
use App\Model;
use Faker\Generator as Faker;

$factory->define(CreditNote::class, function (Faker $faker) {
    return [
        'note' => $faker->sentence(),
        'payment_type' => $faker->boolean(),
        // 'document_id' => 2,
        // 'document_id' => Document::orderByRaw('RAND()')->first()->id
    ];
});
