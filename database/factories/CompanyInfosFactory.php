<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CompanyInfos;
use App\Model;
use App\User;
use Faker\Generator as Faker;

$foo = 1;

$factory->define(CompanyInfos::class, function (Faker $faker) {

    static $order = 3;   

    return [
        'company_name'  => $faker->name().' ltd',
        'owner' => $faker->name(),
        'bulstat' => $faker->numberBetween(1000000000,2147483647),
        'user_id' => $order++,
        // 'accountant_id' => 1,
        'accountant_id' => User::where('isClient',null)->orderByRaw('RAND()')->first()->id,
        // 'user_id' => User::where('isClient',!NULL)->orderByRaw('RAND()')->first()->id,
        // 'user_id' => User::orderByRaw('RAND()')->first()->id
    ];
});
