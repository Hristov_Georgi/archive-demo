<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditNote extends Model
{

    protected $fillable = [
      'note', 'payment_type'  
    ];

    public $fields = [
        'payment type',
        'note',
    ];

    public function getFields()
    {
        return [
            'payment_type',
            'note',
        ];
    }

    public function getPaymentType()
    {
        $map = [
            '1' => 'cash',
            '2' => 'bank',
        ];
        return $map[$this->payment_type];
    }

    public function document()
    {
        return $this->morphOne('App\Document','typeable');
    }
}
