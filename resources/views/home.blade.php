@extends('layouts.app')

@push('laravel-style')
    <link href="{{ asset('css/laravel.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container">
    <div class="content col-lg-5 mx-auto">
        
        @if (currentUser()->isAdmin())

            <div class="h1 my-5 links">
                <a href="{{ route('clients.index') }}">Your Clients</a>
            </div>
        
        @else
            <div class="h1 my-5 links">
                <a href="{{ route('upload.index') }}">Upload Documents</a> {{-- TODO: use policy --}}
            </div>
    
            <div class="h1 my-5 links" >
                <a href="{{ route('uploads.index') }}">Uploaded Documents</a>
            </div>
            
            <div class="h1 my-5 links">
                <a href="{{ route('stored-docs.show', getCurrentUserCompany()) }}">Filled Documents</a>
            </div>
        @endif
        
       
    </div>
</div>
    
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
