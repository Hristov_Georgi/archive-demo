# Archive Demo

    Laravel 7.29.3 Project. An app designed to facilitate the work of accounting houses and their respected customers.

## Quick Start

## Install Dependencies
composer install

## Run Migrations
php artisan migrate

## Import Predefined users
php artisan db:seed

## Add virtual host if using Apache or just use laravel build in server

## If you get an error about an encryption key
php artisan key:generate

## Endpoints

List all articles with links and meta

Welcome Page 

GET /

Main Page 

GET /home

Upload Documents (when logged as a client user)

GET /upload

List of uploaded documents by user themselves

GET /uploads

List of Accounting Admin clients (when logged as an admin)

GET /clients

Show individual client

GET /clients/{id}

Register new client

POST /clients

View particular upload

GET /file/view/{file}
    
Download particular upload

GET /file/download/{file}

Create form for accounting document

GET /clients/{client}/documents/create?{uploadId}

Persist to the database filled document

POST /clients/{client}/documents



## App Info


### Author

Georgi Hristov

### Version

1.0.0

### License

This project is licensed under the MIT License

