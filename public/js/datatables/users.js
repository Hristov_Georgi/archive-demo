
$(document).ready(function () {
    $('#clients-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "/clients",
            type: 'GET'
        },
        columns: [

            {data: 'id', name: 'id'},

            {data: 'company_name', name: 'company_name'},

            {data: 'bulstat', name: 'bulstat'},

            {data: 'action', name: 'action', orderable: false, searchable: false},

            {data: 'unfilled' }
            ],
        scrollY: "350px",

        columnDefs: [
            {"className": "dt-center", "targets": "_all"}
          ]
    })
});