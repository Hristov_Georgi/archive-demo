<?php

namespace App\Http\Controllers;

use App\FileUpload;
use Illuminate\Http\Request;

class DropzoneController extends Controller
{
    public function index()
    {
        return view('dropzone.index');
    }

    public function store(Request $request)
    {
        $image = $request->file('file');
        $imageName = time().'-'.$image->getClientOriginalName();
        $path = $image->storeAs('documents',$imageName);  // a separete directories could be made for diff file types
        

        FileUpload::create([
            'path' => $path,
            'extension' => $image->extension(),
            'company_id' => currentUser()->getCompanyInfo->id,
        ]);
        
        return response()->json(['success' => $imageName]);
    }
}
