<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    protected $fillable = [
        'tax_base',
        'currency',
        'price', 
        'vat',
    ];


    public $fields = [
        'tax base',
        'currency',
        'price', 
        'vat',
    ];

    public function getFields()
    {
        return [
            '1' => 'tax base',
            '2' =>'currency',
            '3' =>'price', 
            '4' =>'vat',
        ];
    }

    public function document()
    {
        return $this->morphOne('App\Document','typeable');
    }
}
