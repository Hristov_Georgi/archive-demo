<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{

    protected $fillable = [
        'number', 'date', 'company_id', 'upload_id'
    ];

    public $documentTypeFields = [
        '1' => 'invoice',
        '3' => 'bank statement',
        '2' => 'credit note',
    ];


    public function client()
    {
        return $this->belongsTo(CompanyInfos::class,'company_id');
    }

    public function typeable()
    {
        return $this->morphTo();
        // return $this->morphTo(__FUNCTION__, 'typeable_type', 'typeable_id');
    }

    public function getTypeName()
    {
        $map = [
            'Invoice' => 'Invoice',
            'BankStatement' => 'Bank Statement',
            'CreditNote' => 'Credit Note',
        ];

        return $map[$this->typeable_type];
    }

}
