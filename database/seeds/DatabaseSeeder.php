<?php

use App\CompanyInfos;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(CompanyInfosSeeder::class);
        $this->call(DocumentSeeder::class);
        $this->call(InvoiceSeeder::class);
        $this->call(CreditNoteSeeder::class);
        $this->call(BankStatementSeeder::class);
       
    }
}
