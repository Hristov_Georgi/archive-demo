
@extends('layouts.app')

@push('dropzone.css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css"/>
@endpush


@section('content')
    <div class="content">
        <div class="row">
            <div class="col">
                <form action="{{ route('upload.store') }}"
                class="dropzone"
                method="post"
                enctype="multipart/form-data"
                id="uploadImage"
                >
                @csrf
                </form>
            </div>
        </div>
    </div>

    
    @push('dropzone-scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js"></script>
        <script>
            Dropzone.options.uploadImage = {
                maxFilesize : 1,
            }
        </script>
    @endpush

@endsection
