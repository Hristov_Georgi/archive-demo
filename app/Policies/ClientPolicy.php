<?php

namespace App\Policies;

use App\CompanyInfos;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\CompanyInfos  $companyInfos
     * @return mixed
     */
    public function view(User $user, CompanyInfos $client)
    {
        return $user->id == $client->accountant_id || $client->user_id == $user->id ;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }


    public function viewUserStoredDocs(User $currUser, CompanyInfos $company)
    {
        return $currUser->id == $company->user_id; 
    }

}
