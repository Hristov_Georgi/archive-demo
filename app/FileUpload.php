<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileUpload extends Model
{
    protected $guarded = [];

    public function owner()
    {
        return $this->belongsTo(CompanyInfos::class,'company_id');
    }
}
