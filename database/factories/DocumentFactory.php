<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\CompanyInfos;
use App\Document;
use App\User;
use Faker\Generator as Faker;

$factory->define(Document::class, function (Faker $faker) {
    return [
        // 'typeable_type' => $faker->randomElement($array = array ('App\Invoice','App\CreditNote','App\BankStatement')),
        'typeable_type' => $faker->randomElement($array = array ('Invoice','CreditNote','BankStatement')),
        'typeable_id' => 1,
        'number' => $faker->numberBetween(1,200000),
        'date' => $faker->date,
        'company_id' => CompanyInfos::orderByRaw('RAND()')->first()->id
        // 'company_id' => User::where('isClient',!null)->orderByRaw('RAND()')->first()->id
        
    ];
});
