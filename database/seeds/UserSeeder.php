<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Admin Adminnov';
        $user->email = 'admin@admin.com';
        $user->password = bcrypt(12341234);
    
        $user->save();

        $user2 = new User();
        $user2->name = 'admin 2';
        $user2->email = 'admin2@admin.com';
        $user2->password = bcrypt(12341234);
    
        $user2->save();

        $user3 = new User();
        $user3->name = 'Client Clientov';
        $user3->email = 'client@client.com';
        $user3->password = bcrypt(12341234);
        $user3->isClient = true;

        $user3->save();
        
        $user4 = new User();
        $user4->name = 'Client 2';
        $user4->email = 'client2@client.com';
        $user4->password = bcrypt(12341234);
        $user4->isClient = true;
        
        $user4->save();
        
       
        factory(User::class,3)->create();
    }
}
