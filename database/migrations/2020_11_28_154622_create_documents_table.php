<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {

            $table->id();
            $table->morphs('typeable');
            $table->unique(['typeable_type','typeable_id']);
      
            $table->string('number');
            $table->date('date');
            $table->timestamps();
            
            $table->unsignedBigInteger('upload_id');                                    
            // $table->foreign('upload_id')->references('id')->on('file_uploads'); 

            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('company_infos')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
